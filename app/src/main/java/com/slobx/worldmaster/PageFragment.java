package com.slobx.worldmaster;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by slobx on 5/31/2017.
 */

public class PageFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    Context context;
    GridView gridView;
    GridViewUnlockAdapter adapter;
    String[] countriesStrings = {"europe", "asia", "africa", "northamerica", "southamerica", "oceania", "unrecognized", "territories"};


    public static PageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.countries_unlock, container, false);
        adapter = new GridViewUnlockAdapter(view.getContext(), mPage);
        gridView = (GridView) view.findViewById(R.id.myGrid);
        gridView.setAdapter(adapter);
        context = container.getContext();


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (mPage) {
                    case 1:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 2:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 3:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 4:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 5:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 6:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 7:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                    case 8:
                        if (GridViewUnlockAdapter.continentsLockedPositions.get(mPage - 1).contains(position))
                            startUnlockDialog(mPage - 1, position);
                        else {
                            startInfoDialog(mPage - 1, position);
                        }
                        break;
                }
            }
        });


        return view;
    }


    @Override
    public View getView() {
        return super.getView();
    }

    private void startUnlockDialog(final int continent, final int country) {


        final String countryString = CountryItems.continentsNames[continent][country];
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Unlock new country")
                .setContentText("Do you want to unlock new country? It will cost you 100e")
                .setConfirmText("Yes, I want it!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog
                                .setTitleText("Unlocked!")
                                .setContentText("You unlocked " + countryString)
                                .setConfirmText("Back")
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        removeFlagFromArray(continent, country);
                        adapter.notifyDataSetChanged();


                    }
                }).show();


    }

    private void startInfoDialog(int continent, int country) {
        new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText(CountryItems.continentsNames[continent][country])
                .setCustomImage(GridViewUnlockAdapter.continentsIDs[continent][country])
                .setContentText(GridViewUnlockAdapter.continentsInfo[continent][country])
                .setConfirmText("OK")
                .setConfirmClickListener(null).show();


    }

    private void removeFlagFromArray(int continent, int country) {
        ArrayList<Integer> customList = new ArrayList<>();
        switch (continent) {
            case 0:
                customList = SavePref.loadArrayList(context, countriesStrings[0]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[0], customList);
                customList.clear();
                break;
            case 1:
                customList = SavePref.loadArrayList(context, countriesStrings[1]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[1], customList);
                customList.clear();
                break;
            case 2:
                customList = SavePref.loadArrayList(context, countriesStrings[2]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[2], customList);
                customList.clear();
                break;
            case 3:
                customList = SavePref.loadArrayList(context, countriesStrings[3]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[3], customList);
                customList.clear();
                break;
            case 4:
                customList = SavePref.loadArrayList(context, countriesStrings[4]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[4], customList);
                customList.clear();
                break;
            case 5:
                customList = SavePref.loadArrayList(context, countriesStrings[5]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[5], customList);
                customList.clear();
                break;
            case 6:
                customList = SavePref.loadArrayList(context, countriesStrings[6]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[6], customList);
                customList.clear();
                break;
            case 7:
                customList = SavePref.loadArrayList(context, countriesStrings[7]);
                customList.remove(new Integer(country));
                SavePref.saveArrayList(context, countriesStrings[7], customList);
                customList.clear();
                break;
        }

    }

    //TODO finish problem with updating the text in title bars - notifydatasetchange doesnt work, find out why

}
