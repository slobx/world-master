package com.slobx.worldmaster;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import java.util.ArrayList;
import java.util.Random;

import io.netopen.hotbitmapgg.library.view.RingProgressBar;

/**
 * Created by Slobodan on 2/23/2017.
 */

public class GameActivity extends AppCompatActivity {

    ImageView flag1,flag2, flag3, flag4;
    TextView questionTextView;
    TextView scoreTextView;
    TextView bubbleTV1, bubbleTV2, bubbleTV3, bubbleTV4;
    CountryItems countryItems;
    int correctAnswer;
    int locationOfCorrectAnswer;
    int score = 0;
    int scoreMultiplier = 1;
    View gridLayout;
    RingProgressBar mRingProgressBar;
    AnimationClass animation;
    boolean timerStarted;
    boolean isGameOver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        countryItems = new CountryItems();
        animation = new AnimationClass();

        flag1 = (ImageView) findViewById(R.id.imageView1);
        flag2 = (ImageView) findViewById(R.id.imageView2);
        flag3 = (ImageView) findViewById(R.id.imageView3);
        flag4 = (ImageView) findViewById(R.id.imageView4);

        bubbleTV1 = (TextView) findViewById(R.id.textView1);
        bubbleTV2 = (TextView) findViewById(R.id.textView2);
        bubbleTV3 = (TextView) findViewById(R.id.textView3);
        bubbleTV4 = (TextView) findViewById(R.id.textView4);

        bubbleTV1.setText(CountryItems.LEVEL1_FLAG_VALUE);
        bubbleTV2.setText(CountryItems.LEVEL2_FLAG_VALUE);
        bubbleTV3.setText(CountryItems.LEVEL3_FLAG_VALUE);
        bubbleTV4.setText(CountryItems.LEVEL3_FLAG_VALUE);

        questionTextView = (TextView) findViewById(R.id.questionTextView);
        scoreTextView = (TextView) findViewById(R.id.scoreTextView);
        scoreTextView.setText(String.valueOf(score));
        YoYo.with(Techniques.Bounce).duration(300).playOn(scoreTextView);

        gridLayout = findViewById(R.id.gridLayoutFlags);
        mRingProgressBar = (RingProgressBar) findViewById(R.id.progress_bar);
        timerStarted = false;
        isGameOver = false;

        setFlags();
    }

    public void checkClickedFlag(View view) {

        float viewHeight = view.getHeight();
        float viewWidth = view.getWidth();
        android.os.Handler handler = new android.os.Handler();

        switch (view.getId()) {
            case R.id.imageView1:
                if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {

                    animation.correctAnswerAnimation(viewWidth/2 - flag1.getPaddingRight()*3,viewHeight/2- flag1.getPaddingBottom()*2, flag1);
                    flag1.setClickable(false);
                    flag2.setClickable(false);
                    flag3.setClickable(false);
                    flag4.setClickable(false);
                    animation.wrongAnswerAnimation(flag2);
                    animation.wrongAnswerAnimation(flag3);
                    animation.wrongAnswerAnimation(flag4);
                    bubbleTV1.setVisibility(View.VISIBLE);
                    animation.bubblePathAnimation(bubbleTV1, flag1);
                    score = (score + 100)*scoreMultiplier;
                    scoreTextView.setText(String.valueOf(score));
                    YoYo.with(Techniques.Bounce).duration(300).playOn(scoreTextView);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isGameOver) {
                                setFlags();
                            }


                        }
                    }, 501);
                }
                else
                    animation.scaleWrongAnswer(flag1);
                break;
            case R.id.imageView2:
                if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {

                    animation.correctAnswerAnimation(-viewWidth/2 - flag2.getPaddingLeft()/2,viewHeight/2- flag2.getPaddingBottom()*2, flag2);

                    flag1.setClickable(false);
                    flag2.setClickable(false);
                    flag3.setClickable(false);
                    flag4.setClickable(false);
                    animation.wrongAnswerAnimation(flag1);
                    animation.wrongAnswerAnimation(flag3);
                    animation.wrongAnswerAnimation(flag4);
                    bubbleTV2.setVisibility(View.VISIBLE);
                    animation.bubblePathAnimation(bubbleTV2, flag2);
                    score = (score + 100)*scoreMultiplier;
                    scoreTextView.setText(String.valueOf(score));
                    YoYo.with(Techniques.Bounce).duration(300).playOn(scoreTextView);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isGameOver) {
                                setFlags();
                            }

                        }
                    }, 501);
                }
                else
                    animation.scaleWrongAnswer(flag2);
                break;
            case R.id.imageView3:
                if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {

                    animation.correctAnswerAnimation(viewWidth/2 - flag3.getPaddingRight()*3,-viewHeight/2, flag3);

                    flag1.setClickable(false);
                    flag2.setClickable(false);
                    flag3.setClickable(false);
                    flag4.setClickable(false);
                    animation.wrongAnswerAnimation(flag1);
                    animation.wrongAnswerAnimation(flag2);
                    animation.wrongAnswerAnimation(flag4);
                    bubbleTV3.setVisibility(View.VISIBLE);
                    animation.bubblePathAnimation(bubbleTV3, flag3);
                    score = (score + 100)*scoreMultiplier;
                    scoreTextView.setText(String.valueOf(score));
                    YoYo.with(Techniques.Bounce).duration(300).playOn(scoreTextView);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isGameOver) {
                                setFlags();
                            }

                        }
                    }, 500);
                }
                else
                    animation.scaleWrongAnswer(flag3);
                break;
            case R.id.imageView4:
                if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {

                    animation.correctAnswerAnimation(-viewWidth/2 - flag4.getPaddingLeft()/2,-viewHeight/2, flag4);
                    flag1.setClickable(false);
                    flag2.setClickable(false);
                    flag3.setClickable(false);
                    flag4.setClickable(false);
                    animation.wrongAnswerAnimation(flag1);
                    animation.wrongAnswerAnimation(flag2);
                    animation.wrongAnswerAnimation(flag3);
                    bubbleTV4.setVisibility(View.VISIBLE);
                    animation.bubblePathAnimation(bubbleTV4, flag4);
                    score = (score + 100)*scoreMultiplier;
                    scoreTextView.setText(String.valueOf(score));
                    YoYo.with(Techniques.Bounce).duration(300).playOn(scoreTextView);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isGameOver) {
                                setFlags();
                            }

                        }
                    }, 501);
                }
                else
                    animation.scaleWrongAnswer(flag4);
                break;
        }
    }

    public void setFlags(){

        bubbleTV1.setVisibility(View.INVISIBLE);
        bubbleTV2.setVisibility(View.INVISIBLE);
        bubbleTV3.setVisibility(View.INVISIBLE);
        bubbleTV4.setVisibility(View.INVISIBLE);

        //this is where I can change timer - spped up if I need to test - default:30000
        CountDownTimer timer = new CountDownTimer(30000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                int prog = 300-(int) millisUntilFinished/100;
                mRingProgressBar.setProgress(prog);
                mRingProgressBar.setOnProgressListener(new RingProgressBar.OnProgressListener() {

                    @Override
                    public void progressToComplete() {
                        // Progress reaches the maximum callback default Max value is 100

                    }
                });
            }

            @Override
            public void onFinish() {
                isGameOver = true;
                animation.moveFromTheScreenAnimation(flag1, flag2, flag3, flag4, questionTextView, mRingProgressBar, scoreTextView);
                flag1.setClickable(false);
                flag2.setClickable(false);
                flag3.setClickable(false);
                flag4.setClickable(false);
                createGameOverDialog();
            }

        };

        if(!timerStarted) {
            timer.start();
            timerStarted = true;
        }
        flag1.setClickable(true);
        flag2.setClickable(true);
        flag3.setClickable(true);
        flag4.setClickable(true);

        ArrayList<Integer> answerFlagPositions = new ArrayList<>(countryItems.flagsList.size());

        Random rand = new Random();
        countryItems.setFlagsList();
        countryItems.setCapitalsList();
        countryItems.setNamesList();

        locationOfCorrectAnswer = rand.nextInt(4);
        int wrongAnswers;
        correctAnswer = rand.nextInt(countryItems.flagsList.size());
        questionTextView.setText(countryItems.countriesList.get(correctAnswer));
        YoYo.with(Techniques.StandUp).duration(600).repeat(1).playOn(questionTextView);

        for (int i=0; i<4; i++) {
            if (i == locationOfCorrectAnswer) {
                answerFlagPositions.add(locationOfCorrectAnswer,correctAnswer);
            } else {
                wrongAnswers = rand.nextInt(countryItems.flagsList.size());

                while (wrongAnswers == correctAnswer) {
                    wrongAnswers = rand.nextInt(countryItems.flagsList.size());
                }
                answerFlagPositions.add(wrongAnswers);
            }
        }
        flag1.setImageResource(countryItems.flagsList.get(answerFlagPositions.get(0)));
        flag2.setImageResource(countryItems.flagsList.get(answerFlagPositions.get(1)));
        flag3.setImageResource(countryItems.flagsList.get(answerFlagPositions.get(2)));
        flag4.setImageResource(countryItems.flagsList.get(answerFlagPositions.get(3)));
    }

    private void createGameOverDialog() {
        NiftyDialogBuilder gameOverDialog = NiftyDialogBuilder.getInstance(this);
        gameOverDialog.withTitle("GAME OVER")
                .withMessage("You scored " + String.valueOf(score))
                .withDialogColor("#1c90ec")
                .withDividerColor("#0e4876")
                .withButton1Text("Restart game")
                .withButton2Text("Go to main menu")
                .withDuration(700)
                .withEffect(Effectstype.Slidetop)
                .isCancelable(false)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(GameActivity.this, GameActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .show();
    }




}
