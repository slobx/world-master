package com.slobx.worldmaster;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Created by slobx on 6/11/2017.
 */

public class SavePref {



    public static void saveArrayList(Context context, String key, ArrayList<Integer> values){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        StringBuilder str = new StringBuilder();
        for (int i=0; i < values.size(); i++){
            str.append(values.get(i)).append(",");
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, str.toString());
        editor.commit();    }

    public static ArrayList<Integer> loadArrayList(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String savedString = sharedPreferences.getString(key, "");
        StringTokenizer st = new StringTokenizer(savedString, ",");
        ArrayList<Integer> array = new ArrayList<>();
        while (st.hasMoreTokens()) {
            array.add(Integer.parseInt(st.nextToken()));
        }
        return array;
    }

    public static void saveBoolean(Context context, String key, boolean booleanValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, booleanValue);
        editor.commit();
    }

    public static Boolean loadBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean booleanValue = sharedPreferences.getBoolean(key, true);
        return booleanValue;
    }

    public static void saveInt(Context context, String key, int intValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, intValue);
        editor.commit();
    }

    public static Integer loadInt(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int intValue = sharedPreferences.getInt(key, 0);
        return intValue;
    }


}
