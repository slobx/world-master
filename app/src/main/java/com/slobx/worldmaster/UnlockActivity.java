package com.slobx.worldmaster;


import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.ogaclejapan.smarttablayout.SmartTabLayout;


public class UnlockActivity extends AppCompatActivity {

    public static MyFragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unlock);

        adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(), getApplicationContext());


        ViewPager pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.tabs);
        viewPagerTab.setViewPager(pager);






    }
    public static void updateAdapter(){
        adapter.notifyDataSetChanged();
    }

}

