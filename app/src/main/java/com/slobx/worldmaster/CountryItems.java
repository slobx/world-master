package com.slobx.worldmaster;

import java.util.ArrayList;

/**
 * Created by slobx on 3/3/2017.
 */

public class CountryItems {

    public static final String LEVEL1_FLAG_VALUE = "+1";
    public static final String LEVEL2_FLAG_VALUE = "+2";
    public static final String LEVEL3_FLAG_VALUE = "+3";


    public ArrayList<Integer> flagsList = new ArrayList<>();
    public ArrayList<String> capitalsList = new ArrayList<String>();
    public ArrayList<String> countriesList = new ArrayList<String>();

    public static String[] europeCountriesNames = {"Russian Federation", "France", "Spain", "Sweden", "Norway",
            "Finland", "Poland", "Italy", "United Kingdom", "Greece", "Iceland", "Austria", "Czech Republic", "Denmark", "Switzerland",
            "Ukraine", "Romania", "Belarus", "Kazakhstan", "Bulgaria", "Hungary", "Portugal", "Serbia",
            "Republic of Ireland", "Lithuania", "Latvia", "Croatia", "Bosnia & Herzegovina", "Slovakia", "Estonia",
            "Netherlands", "Republic of Moldova", "Belgium", "Albania", "Republic of Macedonia", "Turkey",
            "Slovenia", "Cyprus", "Luxembourg", "Andorra", "Malta", "Liechtenstein", "San Marino", "Monaco",
            "Germany", "Montenegro", "Georgia", "Vatican City"};

    public String[] europeCapitalsNames = {"Moscow", "Paris", "Madrid", "Stockholm", "Oslo",
            "Helsinki", "Warsaw", "Rome", "London", "Athens", "Reykjavik", "Vienna", "Prague", "Copenhagen", "Bern",
            "Kiev", "Bucharest", "Minsk", "Astana", "Sofia", "Budapest", "Lisbon", "Belgrade",
            "Dublin", "Vilnius", "Riga", "Zagreb", "Sarajevo", "Bratislava", "Tallinn",
            "Amsterdam", "Chișinau", "Brussels", "Tirana", "Skopje", "Ankara",
            "Ljubljana", "Nicosia", "Luxembourg", "Andorra la Vella", "Valletta", "Vaduz",
            "San Marino", "Monaco", "Berlin", "Podgorica", "Tbilisi", "Vatican city"};

    public static String[] asiaCountriesNames = {"Afghanistan", "China", "India", "Indonesia", "Iran", "Iraq", "Israel",
            "Japan", "Malaysia", "Democratic People's Republic of Korea",
            "Philippines", "Qatar", "Saudi Arabia", "Republic of Korea", "United Arab Emirates", "Azerbaijan",
            "Bahrain", "Bangladesh", "Brunei", "Cambodia", "Armenia", "Bhutan", "Jordan", "Kuwait", "Kyrgyzstan",
            "Lao People's Democratic Republic", "Lebanon", "Maldives", "Mongolia", "Myanmar", "Nepal", "Oman",
            "Pakistan", "Singapore", "Sri Lanka", "Syrian Arab Republic", "Tajikistan",
            "Thailand", "Turkmenistan", "Uzbekistan", "Vietnam", "Yemen"};

    public String[] asiaCapitalsNames = {"Kabul", "Beijing", "New Delhi", "Jakarta", "Tehran", "Baghdad",
            "Jerusalem", "Tokyo", "Kuala Lumpur", "Pyongyang",
            "Manila", "Doha", "Riyadh", "Seoul", "Abu Dhabi", "Baku", "Manama", "Dhaka", "Bandar Seri Begawan",
            "Phnom Penh", "Yerevan", "Thimphu", "Amman", "Kuwait City", "Bishkek", "Vientiane", "Beirut",
            "Male", "Ulan Bator", "Naypyidaw", "Kathmandu", "Muscat", "Islamabad", "Singapore", "Colombo",
            "Damascus", "Dushanbe", "Bangkok", "Ashgabat", "Tashkent", "Hanoi", "Sana'a"};

    public static String[] africaCountriesNames = {"Algeria", "Angola", "Cameroon", "Congo", "Cote d'Ivoire", "Egypt",
            "Ghana", "Kenya", "Mauritius", "Morocco", "Nigeria", "Senegal", "Tunisia",
            "Ethiopia", "Benin", "Botswana", "Burkina Faso", "Burundi", "Cape Verde", "Central African Republic",
            "Chad", "Comoros", "Equatorial Guinea", "Eritrea", "Gabon", "Gambia", "Guinea", "Guinea Bissau",
            "Lesotho", "Liberia", "Madagascar", "Malawi", "Mali", "Mauritania", "Mozambique", "Namibia",
            "Rwanda", "Sao Tome and Principe", "Seychelles", "Sierra Leone", "Somalia", "Sudan", "Swaziland", "Tanzania"};

    public String[] africaCapitalsNames = {"Algiers", "Luanda", "Yaounde", "Brazzaville", "Yamoussoukro",
            "Cairo", "Accra", "Nairobi", "Port Louis", "Rabat", "Abuja", "Dakar", "Tunis",
            "Addis Ababa", "Porto-Novo", "Gaborone", "Ouagadougou", "Bujumbura", "Praia", "Bangui",
            "N'Djamena", "Moroni", "Malabo", "Asmara", "Libreville", "Banjul", "Conakry", "Bissau", "Maseru",
            "Monrovia", "Antananarivo", "Lilongwe", "Bamako", "Nouakchott", "Maputo", "Windhoek",
            "Kigali", "Sao Tome", "Victoria", "Freetown", "Mogadishu", "Khartoum", "Mbabane", "Dodoma"};

    public static String[] northAmericaCountriesNames = {"United States of America", "Bahamas", "Mexico", "Costa Rica", "Cuba", "Jamaica", "Barbados", "Trinidad and Tobago",
            "Antigua and Barbuda", "Panama", "Dominica", "Grenada", "Saint Lucia", "Saint Vincent and the Grenadines",
            "Belize", "El Salvador", "Guatemala", "Honduras", "Nicaragua", "Haiti"};

    public String[] northAmericaCapitalsNames = {"Washington", "Nassau", "Mexico City", "San Jose",
            "Havana", "Kingston", "Bridgetown", "Port of Spain",
            "Saint John's", "Panama City", "Roseau", "St. George's", "Castries", "Kingstown",
            "Belmopan", "San Salvador", "Guatemala City", "Tegucigalpa", "Managua", "Port-au-Prince"};

    public static String[] southAmericaCountriesNames = {"Argentina", "Brazil", "Colombia", "Bolivia", "Chile", "Ecuador", "Guyana",
            "Paraguay", "Peru", "Uruguay", "Venezuela"};

    public String[] southAmericaCapitalsNames = {"Buenos Aires", "Brasilia", "Bogota", "La Paz",
            "Santiago", "Quito", "Georgetown", "Asuncion", "Lima", "Montevideo", "Caracas"};

    public static String[] oceaniaCountriesNames = {"Australia", "Papua New Guinea", "New Zealand", "Fiji", "Solomon Islands",
            "Vanuatu", "Samoa", "Kiribati", "Tonga", "Marshall Islands", "Palau", "Nauru"};

    public static String[] oceaniaCapitalsNames = {"Canberra", "Port Moresby", "Wellington", "Suva", "Honiara",
            "Port Vila", "Apia", "South Tarawa", "Nukuʻalofa", "Majuro", "Ngerulmud", "Yaren"};

    public static String[] unrecognizedCountriesNames = {"Abkhazia",
            "Kosovo",
            "Northern Cyprus",
            "Palestine",
            "Western Sahara",
            "South Ossetia",
            "Nagorno-Karabakh Republic",
            "Pridnestrovian Moldavian Republic",
            "Somaliland",
    };

    public static String[] territoriesCountriesNames = {
        "Christmas Island",
        "Cocos Islands",
        "Norfolk Island",
        "Easter Island",
        "Hong Kong",
        "Macau",
        "Faroe Islands",
        "Greenland",
        "Åland Islands",
        "French Southern and Antarctic Lands",
        "French Polynesia",
        "Wallis and Futuna",
        "Saint Barthelemy",
        "Saint Pierre and Miquelon",
        "New Caledonia",
        "Aruba",
        "Curaçao",
        "Sint Maarten",
        "Bonaire",
        "Sint Eustatius",
        "Saba",
        "Cook Islands",
        "Niue",
        "Tokelau",
        "Alderney",
        "Guernsey",
        "Herm",
        "Isle of Man",
        "Jersey",
        "Sark",
        "Anguilla",
        "Ascension Island",
        "Bermuda",
        "British Antarctic Territory",
        "British Indian Ocean Territory",
        "British Virgin Islands",
        "Cayman Islands",
        "Falkland Islands",
        "Gibraltar",
        "Montserrat",
        "Pitcairn Islands",
        "Saint Helena",
        "South Georgia and the South Sandwich Islands",
        "Tristan da Cunha",
        "Turks and Caicos Islands",
        "American Samoa",
        "Guam",
        "Northern Mariana Islands",
        "Puerto Rico",
        "U.S. Virgin Islands",
    };

    public static String[][] continentsNames = {europeCountriesNames, asiaCountriesNames, africaCountriesNames,
            northAmericaCountriesNames, southAmericaCountriesNames, oceaniaCountriesNames, unrecognizedCountriesNames,territoriesCountriesNames};


    //European unlocked countries flags

    public void setFlagsList() {
        //European unlocked countries flags
        flagsList.add(0, R.drawable.russianfederation);
        flagsList.add(1, R.drawable.france);
        flagsList.add(2, R.drawable.spain);
        flagsList.add(3, R.drawable.sweden);
        flagsList.add(4, R.drawable.norway);
        flagsList.add(5, R.drawable.finland);
        flagsList.add(6, R.drawable.poland);
        flagsList.add(7, R.drawable.italy);
        flagsList.add(8, R.drawable.greatbritain);
        flagsList.add(9, R.drawable.greece);
        flagsList.add(10, R.drawable.iceland);
        flagsList.add(11, R.drawable.austria);
        flagsList.add(12, R.drawable.czechrepublic);
        flagsList.add(13, R.drawable.denmark);
        flagsList.add(14, R.drawable.switzerland);
        //Asia unlocked countries flags
        flagsList.add(15, R.drawable.afghanistan);
        flagsList.add(16, R.drawable.china);
        flagsList.add(17, R.drawable.india);
        flagsList.add(18, R.drawable.indonesia);
        flagsList.add(19, R.drawable.islamicrepublicofiran);
        flagsList.add(20, R.drawable.iraq);
        flagsList.add(21, R.drawable.israel);
        flagsList.add(22, R.drawable.japan);
        flagsList.add(23, R.drawable.malaysia);
        flagsList.add(24, R.drawable.democraticpeoplesrepublicofkorea);
        flagsList.add(25, R.drawable.philippines);
        flagsList.add(26, R.drawable.qatar);
        flagsList.add(27, R.drawable.saudiarabia);
        flagsList.add(28, R.drawable.republicofkorea);
        flagsList.add(29, R.drawable.unitedarabemirates);
        //Africa unlocked countries flags
        flagsList.add(30, R.drawable.algeria);
        flagsList.add(31, R.drawable.angola);
        flagsList.add(32, R.drawable.cameroon);
        flagsList.add(33, R.drawable.congo);
        flagsList.add(34, R.drawable.cotedivoire);
        flagsList.add(35, R.drawable.egypt);
        flagsList.add(36, R.drawable.ghana);
        flagsList.add(37, R.drawable.kenya);
        flagsList.add(38, R.drawable.mauritius);
        flagsList.add(39, R.drawable.morocco);
        flagsList.add(40, R.drawable.nigeria);
        flagsList.add(41, R.drawable.senegal);
        flagsList.add(42, R.drawable.tunisia);
        flagsList.add(43, R.drawable.ethiopia);
        flagsList.add(44, R.drawable.benin);
        flagsList.add(45, R.drawable.botswana);
        //North America unlocked countries flags
        flagsList.add(46, R.drawable.unitedstates);
        flagsList.add(47, R.drawable.bahama);
        flagsList.add(48, R.drawable.mexico);
        flagsList.add(49, R.drawable.costarica);
        flagsList.add(50, R.drawable.cuba);
        flagsList.add(51, R.drawable.jamaica);
        //South America unlocked countries flags
        flagsList.add(52, R.drawable.argentina);
        flagsList.add(53, R.drawable.brazil);
        flagsList.add(54, R.drawable.colombia);
        //Oceania unlocked countries flags
        flagsList.add(55, R.drawable.australia);
        flagsList.add(56, R.drawable.papuanewguinea);
        flagsList.add(57, R.drawable.newzealand);

        //European locked countries
        flagsList.add(R.drawable.ukraine);
        flagsList.add(R.drawable.romania);
        flagsList.add(R.drawable.belarus);
        flagsList.add(R.drawable.kazakhstan);
        flagsList.add(R.drawable.bulgaria);
        flagsList.add(R.drawable.hungary);
        flagsList.add(R.drawable.portugal);
        flagsList.add(R.drawable.serbia);
        flagsList.add(R.drawable.ireland);
        flagsList.add(R.drawable.lithuania);
        flagsList.add(R.drawable.latvia);
        flagsList.add(R.drawable.croatia);
        flagsList.add(R.drawable.bosniaandherzegovina);
        flagsList.add(R.drawable.slovakia);
        flagsList.add(R.drawable.estonia);
        flagsList.add(R.drawable.netherlands);
        flagsList.add(R.drawable.republicofmoldova);
        flagsList.add(R.drawable.belgium);
        flagsList.add(R.drawable.albania);
        flagsList.add(R.drawable.republicofmacedonia);
        flagsList.add(R.drawable.turkey);
        flagsList.add(R.drawable.slovenia);
        flagsList.add(R.drawable.cyprus);
        flagsList.add(R.drawable.luxembourg);
        flagsList.add(R.drawable.andorra);
        flagsList.add(R.drawable.malta);
        flagsList.add(R.drawable.liechtenstein);
        flagsList.add(R.drawable.sanmarino);
        flagsList.add(R.drawable.monaco);
        flagsList.add(R.drawable.germany);
        flagsList.add(R.drawable.montenegro);
        flagsList.add(R.drawable.georgia);
        flagsList.add(R.drawable.vatican);
        //Asia locked countries
        flagsList.add(R.drawable.azerbaijan);
        flagsList.add(R.drawable.bahrain);
        flagsList.add(R.drawable.bangladesh);
        flagsList.add(R.drawable.bruneidarussalam);
        flagsList.add(R.drawable.cambodia);
        flagsList.add(R.drawable.armenia);
        flagsList.add(R.drawable.bhutan);
        flagsList.add(R.drawable.jordan);
        flagsList.add(R.drawable.kuwait);
        flagsList.add(R.drawable.kyrgyzstan);
        flagsList.add(R.drawable.laopeoplesdemocraticrepublic);
        flagsList.add(R.drawable.lebanon);
        flagsList.add(R.drawable.maldives);
        flagsList.add(R.drawable.mongolia);
        flagsList.add(R.drawable.myanmar);
        flagsList.add(R.drawable.nepal);
        flagsList.add(R.drawable.oman);
        flagsList.add(R.drawable.pakistan);
        flagsList.add(R.drawable.singapore);
        flagsList.add(R.drawable.srilanka);
        flagsList.add(R.drawable.syrianarabrepublic);
        flagsList.add(R.drawable.tajikistan);
        flagsList.add(R.drawable.thailand);
        flagsList.add(R.drawable.turkmenistan);
        flagsList.add(R.drawable.uzbekistan);
        flagsList.add(R.drawable.vietnam);
        flagsList.add(R.drawable.yemen);

        //Africa locked countries
        flagsList.add(R.drawable.burkina_faso);
        flagsList.add(R.drawable.burundi);
        flagsList.add(R.drawable.capeverde);
        flagsList.add(R.drawable.centralafricanrepublic);
        flagsList.add(R.drawable.chad);
        flagsList.add(R.drawable.comoros);
        flagsList.add(R.drawable.equatorialguinea);
        flagsList.add(R.drawable.eritrea);
        flagsList.add(R.drawable.gabon);
        flagsList.add(R.drawable.gambia);
        flagsList.add(R.drawable.guinea);
        flagsList.add(R.drawable.guineabissau);
        flagsList.add(R.drawable.lesotho);
        flagsList.add(R.drawable.liberia);
        flagsList.add(R.drawable.madagascar);
        flagsList.add(R.drawable.malawi);
        flagsList.add(R.drawable.mali);
        flagsList.add(R.drawable.mauritania);
        flagsList.add(R.drawable.mozambique);
        flagsList.add(R.drawable.nambia);
        flagsList.add(R.drawable.rwanda);
        flagsList.add(R.drawable.saotomeandprincipe);
        flagsList.add(R.drawable.seychelles);
        flagsList.add(R.drawable.sierraleone);
        flagsList.add(R.drawable.somalia);
        flagsList.add(R.drawable.sudan);
        flagsList.add(R.drawable.swaziland);
        flagsList.add(R.drawable.tanzania);

        //North America locked countries
        flagsList.add(R.drawable.barbados);
        flagsList.add(R.drawable.trinidadandtobago);
        flagsList.add(R.drawable.antiguaandbarbuda);
        flagsList.add(R.drawable.panama);
        flagsList.add(R.drawable.dominica);
        flagsList.add(R.drawable.grenada);
        flagsList.add(R.drawable.saintlucia);
        flagsList.add(R.drawable.stvincentandgrenadines);
        flagsList.add(R.drawable.belize);
        flagsList.add(R.drawable.elsalvador);
        flagsList.add(R.drawable.guatemala);
        flagsList.add(R.drawable.honduras);
        flagsList.add(R.drawable.nicaragua);
        flagsList.add(R.drawable.haiti);

        //South America locked countries
        flagsList.add(R.drawable.bolivia);
        flagsList.add(R.drawable.chile);
        flagsList.add(R.drawable.ecuador);
        flagsList.add(R.drawable.guyana);
        flagsList.add(R.drawable.paraguay);
        flagsList.add(R.drawable.peru);
        flagsList.add(R.drawable.uruguay);
        flagsList.add(R.drawable.venezuela);

        //Oceania locked countries
        flagsList.add(R.drawable.fiji);
        flagsList.add(R.drawable.solomonislands);
        flagsList.add(R.drawable.vanuatu);
        flagsList.add(R.drawable.samoa);
        flagsList.add(R.drawable.kiribati);
        flagsList.add(R.drawable.tonga);
        flagsList.add(R.drawable.marshallislands);
        flagsList.add(R.drawable.palau);
        flagsList.add(R.drawable.nauru);

        //Unrecognized locked countries
        flagsList.add(R.drawable.abkhazia);
        flagsList.add(R.drawable.kosovo);
        flagsList.add(R.drawable.northerncyprus);
        flagsList.add(R.drawable.palestine);
        flagsList.add(R.drawable.westernsahara);
        flagsList.add(R.drawable.southossetia);
        flagsList.add(R.drawable.nagornokarabakhrepublic);
        flagsList.add(R.drawable.pridnestrovianmoldavianrepublic);
        flagsList.add(R.drawable.somaliland);

        //Others locked countries
        flagsList.add(R.drawable.christmasisland);
        flagsList.add(R.drawable.cocosislands);
        flagsList.add(R.drawable.norfolkisland);
        flagsList.add(R.drawable.easterisland);
        flagsList.add(R.drawable.hongkong);
        flagsList.add(R.drawable.macau);
        flagsList.add(R.drawable.faroeislands);
        flagsList.add(R.drawable.greenland);
        flagsList.add(R.drawable.alandislands);
        flagsList.add(R.drawable.frenchsouthernandantarcticlands);
        flagsList.add(R.drawable.frenchpolynesia);
        flagsList.add(R.drawable.wallisandfutuna);
        flagsList.add(R.drawable.saintbarthelemy);
        flagsList.add(R.drawable.saintpierreandmiquelon);
        flagsList.add(R.drawable.newcaledonia);
        flagsList.add(R.drawable.aruba);
        flagsList.add(R.drawable.curacao);
        flagsList.add(R.drawable.sintmaarten);
        flagsList.add(R.drawable.bonaire);
        flagsList.add(R.drawable.sinteustatius);
        flagsList.add(R.drawable.saba);
        flagsList.add(R.drawable.cookislands);
        flagsList.add(R.drawable.niue);
        flagsList.add(R.drawable.tokelau);
        flagsList.add(R.drawable.alderney);
        flagsList.add(R.drawable.guernsey);
        flagsList.add(R.drawable.herm);
        flagsList.add(R.drawable.isleofman);
        flagsList.add(R.drawable.jersey);
        flagsList.add(R.drawable.sark);
        flagsList.add(R.drawable.anguilla);
        flagsList.add(R.drawable.ascensionisland);
        flagsList.add(R.drawable.bermuda);
        flagsList.add(R.drawable.britishantarcticterritory);
        flagsList.add(R.drawable.britishindianoceanterritory);
        flagsList.add(R.drawable.britishvirginislands);
        flagsList.add(R.drawable.caymanislands);
        flagsList.add(R.drawable.falklandislands);
        flagsList.add(R.drawable.gibraltar);
        flagsList.add(R.drawable.montserrat);
        flagsList.add(R.drawable.pitcairnislands);
        flagsList.add(R.drawable.sainthelena);
        flagsList.add(R.drawable.southgeorgiaandthesouthsandwichislands);
        flagsList.add(R.drawable.tristandacunha);
        flagsList.add(R.drawable.turksandcaicosislands);
        flagsList.add(R.drawable.americansamoa);
        flagsList.add(R.drawable.guam);
        flagsList.add(R.drawable.northernmarianaislands);
        flagsList.add(R.drawable.puertorico);
        flagsList.add(R.drawable.usvirginislands);
    }

    public void setCapitalsList() {

        //unlocked European countries names
        capitalsList.add(0, "Moscow");
        capitalsList.add(1, "Paris");
        capitalsList.add(2, "Madrid");
        capitalsList.add(3, "Stockholm");
        capitalsList.add(4, "Oslo");
        capitalsList.add(5, "Helsinki");
        capitalsList.add(6, "Warsaw");
        capitalsList.add(7, "Rome");
        capitalsList.add(8, "London");
        capitalsList.add(9, "Athens");
        capitalsList.add(10, "Reykjavik");
        capitalsList.add(11, "Vienna");
        capitalsList.add(12, "Prague");
        capitalsList.add(13, "Copenhagen");
        capitalsList.add(14, "Bern");
        //unlocked Asia countries names
        capitalsList.add(15, "Kabul");
        capitalsList.add(16, "Beijing");
        capitalsList.add(17, "New Delhi");
        capitalsList.add(18, "Jakarta");
        capitalsList.add(19, "Tehran");
        capitalsList.add(20, "Baghdad");
        capitalsList.add(21, "Jerusalem");
        capitalsList.add(22, "Tokyo");
        capitalsList.add(23, "Kuala Lumpur");
        capitalsList.add(24, "Pyongyang");
        capitalsList.add(25, "Manila");
        capitalsList.add(26, "Doha");
        capitalsList.add(27, "Riyadh");
        capitalsList.add(28, "Seoul");
        capitalsList.add(29, "Abu Dhabi");
        //unlocked Africa countries names
        capitalsList.add(30, "Algiers");
        capitalsList.add(31, "Luanda");
        capitalsList.add(32, "Yaounde");
        capitalsList.add(33, "Brazzaville");
        capitalsList.add(34, "Yamoussoukro");
        capitalsList.add(35, "Cairo");
        capitalsList.add(36, "Accra");
        capitalsList.add(37, "Nairobi");
        capitalsList.add(38, "Port Louis");
        capitalsList.add(39, "Rabat");
        capitalsList.add(40, "Abuja");
        capitalsList.add(41, "Dakar");
        capitalsList.add(42, "Tunis");
        capitalsList.add(43, "Addis Ababa");
        capitalsList.add(44, "Porto-Novo");
        capitalsList.add(45, "Gaborone");
        //unlocked North America countries names
        capitalsList.add(46, "Washington");
        capitalsList.add(47, "Nassau");
        capitalsList.add(48, "Mexico City");
        capitalsList.add(49, "San Jose");
        capitalsList.add(50, "Havana");
        capitalsList.add(51, "Kingston");
        //unlocked South America countries names
        capitalsList.add(52, "Buenos Aires");
        capitalsList.add(53, "Brasilia");
        capitalsList.add(54, "Bogota");
        //unlocked Oceania countries names
        capitalsList.add(55, "Canberra");
        capitalsList.add(56, "Port Moresby");
        capitalsList.add(57, "Wellington");


        //European locked countries names
        capitalsList.add("Kiev");
        capitalsList.add("Bucharest");
        capitalsList.add("Minsk");
        capitalsList.add("Astana");
        capitalsList.add("Sofia");
        capitalsList.add("Budapest");
        capitalsList.add("Lisbon");
        capitalsList.add("Belgrade");
        capitalsList.add("Dublin");
        capitalsList.add("Vilnius");
        capitalsList.add("Riga");
        capitalsList.add("Zagreb");
        capitalsList.add("Sarajevo");
        capitalsList.add("Bratislava");
        capitalsList.add("Tallinn");
        capitalsList.add("Amsterdam");
        capitalsList.add("Chișinau");
        capitalsList.add("Brussels");
        capitalsList.add("Tirana");
        capitalsList.add("Skopje");
        capitalsList.add("Ankara");
        capitalsList.add("Ljubljana");
        capitalsList.add("Nicosia");
        capitalsList.add("Luxembourg");
        capitalsList.add("Andorra la Vella");
        capitalsList.add("Valletta");
        capitalsList.add("Vaduz");
        capitalsList.add("San Marino");
        capitalsList.add("Monaco");
        capitalsList.add("Berlin");
        capitalsList.add("Podgorica");
        capitalsList.add("Tbilisi");
        capitalsList.add("Vatican city");
        //Asia locked countries
        capitalsList.add("Baku");
        capitalsList.add("Manama");
        capitalsList.add("Dhaka");
        capitalsList.add("Bandar Seri Begawan");
        capitalsList.add("Phnom Penh");
        capitalsList.add("Yerevan");
        capitalsList.add("Thimphu");
        capitalsList.add("Amman");
        capitalsList.add("Kuwait City");
        capitalsList.add("Bishkek");
        capitalsList.add("Vientiane");
        capitalsList.add("Beirut");
        capitalsList.add("Male");
        capitalsList.add("Ulan Bator");
        capitalsList.add("Naypyidaw");
        capitalsList.add("Kathmandu");
        capitalsList.add("Muscat");
        capitalsList.add("Islamabad");
        capitalsList.add("Singapore");
        capitalsList.add("Colombo");
        capitalsList.add("Damascus");
        capitalsList.add("Dushanbe");
        capitalsList.add("Bangkok");
        capitalsList.add("Ashgabat");
        capitalsList.add("Tashkent");
        capitalsList.add("Hanoi");
        capitalsList.add("Sana'a");
        //Africa locked countries
        capitalsList.add("Ouagadougou");
        capitalsList.add("Bujumbura");
        capitalsList.add("Praia");
        capitalsList.add("Bangui");
        capitalsList.add("N'Djamena");
        capitalsList.add("Moroni");
        capitalsList.add("Malabo");
        capitalsList.add("Asmara");
        capitalsList.add("Libreville");
        capitalsList.add("Banjul");
        capitalsList.add("Conakry");
        capitalsList.add("Bissau");
        capitalsList.add("Maseru");
        capitalsList.add("Monrovia");
        capitalsList.add("Antananarivo");
        capitalsList.add("Lilongwe");
        capitalsList.add("Bamako");
        capitalsList.add("Nouakchott");
        capitalsList.add("Maputo");
        capitalsList.add("Windhoek");
        capitalsList.add("Kigali");
        capitalsList.add("Sao Tome");
        capitalsList.add("Victoria");
        capitalsList.add("Freetown");
        capitalsList.add("Mogadishu");
        capitalsList.add("Khartoum");
        capitalsList.add("Mbabane");
        capitalsList.add("Dodoma");
        //North America locked countries
        capitalsList.add("Bridgetown");
        capitalsList.add("Port of Spain");
        capitalsList.add("Saint John's");
        capitalsList.add("Panama City");
        capitalsList.add("Roseau");
        capitalsList.add("St. George's");
        capitalsList.add("Castries");
        capitalsList.add("Kingstown");
        capitalsList.add("Belmopan");
        capitalsList.add("San Salvador");
        capitalsList.add("Guatemala City");
        capitalsList.add("Tegucigalpa");
        capitalsList.add("Managua");
        capitalsList.add("Port-au-Prince");
        //South America locked countries
        capitalsList.add("La Paz");
        capitalsList.add("Santiago");
        capitalsList.add("Quito");
        capitalsList.add("Georgetown");
        capitalsList.add("Asuncion");
        capitalsList.add("Lima");
        capitalsList.add("Montevideo");
        capitalsList.add("Caracas");
        //Oceania locked countries
        capitalsList.add("Suva");
        capitalsList.add("Honiara");
        capitalsList.add("Port Vila");
        capitalsList.add("Apia");
        capitalsList.add("South Tarawa");
        capitalsList.add("Nukuʻalofa");
        capitalsList.add("Majuro");
        capitalsList.add("Ngerulmud");
        capitalsList.add("Yaren");


    }

    public void setNamesList() {
        //unlocked European countries names
        countriesList.add(0, "Russian Federation");
        countriesList.add(1, "France");
        countriesList.add(2, "Spain");
        countriesList.add(3, "Sweden");
        countriesList.add(4, "Norway");
        countriesList.add(5, "Finland");
        countriesList.add(6, "Poland");
        countriesList.add(7, "Italy");
        countriesList.add(8, "United Kingdom");
        countriesList.add(9, "Greece");
        countriesList.add(10, "Iceland");
        countriesList.add(11, "Austria");
        countriesList.add(12, "Czech Republic");
        countriesList.add(13, "Denmark");
        countriesList.add(14, "Switzerland");
        //unlocked Asia countries names
        countriesList.add(15, "Afghanistan");
        countriesList.add(16, "China");
        countriesList.add(17, "India");
        countriesList.add(18, "Indonesia");
        countriesList.add(19, "Iran");
        countriesList.add(20, "Iraq");
        countriesList.add(21, "Israel");
        countriesList.add(22, "Japan");
        countriesList.add(23, "Malaysia");
        countriesList.add(24, "Democratic People's Republic of Korea");
        countriesList.add(25, "Philippines");
        countriesList.add(26, "Qatar");
        countriesList.add(27, "Saudi Arabia");
        countriesList.add(28, "Republic of Korea");
        countriesList.add(29, "United Arab Emirates");
        //unlocked Africa countries names
        countriesList.add(30, "Algeria");
        countriesList.add(31, "Angola");
        countriesList.add(32, "Cameroon");
        countriesList.add(33, "Congo");
        countriesList.add(34, "Cote d'Ivoire");
        countriesList.add(35, "Egypt");
        countriesList.add(36, "Ghana");
        countriesList.add(37, "Kenya");
        countriesList.add(38, "Mauritius");
        countriesList.add(39, "Morocco");
        countriesList.add(40, "Nigeria");
        countriesList.add(41, "Senegal");
        countriesList.add(42, "Tunisia");
        countriesList.add(43, "Ethiopia");
        countriesList.add(44, "Benin");
        countriesList.add(45, "Botswana");
        //unlocked North America countries names
        countriesList.add(46, "United States of America");
        countriesList.add(47, "Bahamas");
        countriesList.add(48, "Mexico");
        countriesList.add(49, "Costa Rica");
        countriesList.add(50, "Cuba");
        countriesList.add(51, "Jamaica");
        //unlocked South America countries names
        countriesList.add(52, "Argentina");
        countriesList.add(53, "Brazil");
        countriesList.add(54, "Colombia");
        //unlocked Oceania countries names
        countriesList.add(55, "Australia");
        countriesList.add(56, "Papua New Guinea");
        countriesList.add(57, "New Zealand");


        //European locked countries names
        countriesList.add("Ukraine");
        countriesList.add("Romania");
        countriesList.add("Belarus");
        countriesList.add("Kazakhstan");
        countriesList.add("Bulgaria");
        countriesList.add("Hungary");
        countriesList.add("Portugal");
        countriesList.add("Serbia");
        countriesList.add("Republic of Ireland");
        countriesList.add("Lithuania");
        countriesList.add("Latvia");
        countriesList.add("Croatia");
        countriesList.add("Bosnia & Herzegovina");
        countriesList.add("Slovakia");
        countriesList.add("Estonia");
        countriesList.add("Netherlands");
        countriesList.add("Republic of Moldova");
        countriesList.add("Belgium");
        countriesList.add("Albania");
        countriesList.add("Republic of Macedonia");
        countriesList.add("Turkey");
        countriesList.add("Slovenia");
        countriesList.add("Cyprus");
        countriesList.add("Luxembourg");
        countriesList.add("Andorra");
        countriesList.add("Malta");
        countriesList.add("Liechtenstein");
        countriesList.add("San Marino");
        countriesList.add("Monaco");
        countriesList.add("Germany");
        countriesList.add("Montenegro");
        countriesList.add("Georgia");
        countriesList.add("Vatican city");

        //Asia locked countries
        countriesList.add("Azerbaijan");
        countriesList.add("Bahrain");
        countriesList.add("Bangladesh");
        countriesList.add("Brunei");
        countriesList.add("Cambodia");
        countriesList.add("Armenia");
        countriesList.add("Bhutan");
        countriesList.add("Jordan");
        countriesList.add("Kuwait");
        countriesList.add("Kyrgyzstan");
        countriesList.add("Lao People's Democratic Republic");
        countriesList.add("Lebanon");
        countriesList.add("Maldives");
        countriesList.add("Mongolia");
        countriesList.add("Myanmar");
        countriesList.add("Nepal");
        countriesList.add("Oman");
        countriesList.add("Pakistan");
        countriesList.add("Singapore");
        countriesList.add("Sri Lanka");
        countriesList.add("Syrian Arab Republic");
        countriesList.add("Tajikistan");
        countriesList.add("Thailand");
        countriesList.add("Turkmenistan");
        countriesList.add("Uzbekistan");
        countriesList.add("Vietnam");
        countriesList.add("Yemen");
        //Africa locked countries
        countriesList.add("Burkina Faso");
        countriesList.add("Burundi");
        countriesList.add("Cape Verde");
        countriesList.add("Central African Republic");
        countriesList.add("Chad");
        countriesList.add("Comoros");
        countriesList.add("Equatorial Guinea");
        countriesList.add("Eritrea");
        countriesList.add("Gabon");
        countriesList.add("Gambia");
        countriesList.add("Guinea");
        countriesList.add("Guinea Bissau");
        countriesList.add("Lesotho");
        countriesList.add("Liberia");
        countriesList.add("Madagascar");
        countriesList.add("Malawi");
        countriesList.add("Mali");
        countriesList.add("Mauritania");
        countriesList.add("Mozambique");
        countriesList.add("Namibia");
        countriesList.add("Rwanda");
        countriesList.add("Sao Tome and Principe");
        countriesList.add("Seychelles");
        countriesList.add("Sierra Leone");
        countriesList.add("Somalia");
        countriesList.add("Sudan");
        countriesList.add("Swaziland");
        countriesList.add("Tanzania");
        //North America locked countries
        countriesList.add("Barbados");
        countriesList.add("Trinidad and Tobago");
        countriesList.add("Antigua and Barbuda");
        countriesList.add("Panama");
        countriesList.add("Dominica");
        countriesList.add("Grenada");
        countriesList.add("Saint Lucia");
        countriesList.add("Saint Vincent and the Grenadines");
        countriesList.add("Belize");
        countriesList.add("El Salvador");
        countriesList.add("Guatemala");
        countriesList.add("Honduras");
        countriesList.add("Nicaragua");
        countriesList.add("Haiti");
        //South America locked countries
        countriesList.add("Bolivia");
        countriesList.add("Chile");
        countriesList.add("Ecuador");
        countriesList.add("Guyana");
        countriesList.add("Paraguay");
        countriesList.add("Peru");
        countriesList.add("Uruguay");
        countriesList.add("Venezuela");
        //Oceania locked countries
        countriesList.add("Fiji");
        countriesList.add("Solomon Islands");
        countriesList.add("Vanuatu");
        countriesList.add("Samoa");
        countriesList.add("Kiribati");
        countriesList.add("Tonga");
        countriesList.add("Marshall Islands");
        countriesList.add("Palau");
        countriesList.add("Nauru");


        //Unrecognized locked countries

        countriesList.add("Abkhazia");
        countriesList.add("Kosovo");
        countriesList.add("Northern Cyprus");
        countriesList.add("Palestine");
        countriesList.add("Western Sahara");
        countriesList.add("South Ossetia");
        countriesList.add("Nagorno-Karabakh Republic");
        countriesList.add("Pridnestrovian Moldavian Republic");
        countriesList.add("Somaliland");

        //Others locked countries

        countriesList.add("Christmas Island");
        countriesList.add("Cocos Islands");
        countriesList.add("Norfolk Island");
        countriesList.add("Easter Island");
        countriesList.add("Hong Kong");
        countriesList.add("Macau");
        countriesList.add("Faroe Islands");
        countriesList.add("Greenland");
        countriesList.add("Åland Islands");
        countriesList.add("French Southern and Antarctic Lands");
        countriesList.add("French Polynesia");
        countriesList.add("Wallis and Futuna");
        countriesList.add("Saint Barthelemy");
        countriesList.add("Saint Pierre and Miquelon");
        countriesList.add("New Caledonia");
        countriesList.add("Aruba");
        countriesList.add("Curaçao");
        countriesList.add("Sint Maarten");
        countriesList.add("Bonaire");
        countriesList.add("Sint Eustatius");
        countriesList.add("Saba");
        countriesList.add("Cook Islands");
        countriesList.add("Niue");
        countriesList.add("Tokelau");
        countriesList.add("Alderney");
        countriesList.add("Guernsey");
        countriesList.add("Herm");
        countriesList.add("Isle of Man");
        countriesList.add("Jersey");
        countriesList.add("Sark");
        countriesList.add("Anguilla");
        countriesList.add("Ascension Island");
        countriesList.add("Bermuda");
        countriesList.add("British Antarctic Territory");
        countriesList.add("British Indian Ocean Territory");
        countriesList.add("British Virgin Islands");
        countriesList.add("Cayman Islands");
        countriesList.add("Falkland Islands");
        countriesList.add("Gibraltar");
        countriesList.add("Montserrat");
        countriesList.add("Pitcairn Islands");
        countriesList.add("Saint Helena");
        countriesList.add("South Georgia and the South Sandwich Islands");
        countriesList.add("Tristan da Cunha");
        countriesList.add("Turks and Caicos Islands");
        countriesList.add("American Samoa");
        countriesList.add("Guam");
        countriesList.add("Northern Mariana Islands");
        countriesList.add("Puerto Rico");
        countriesList.add("U.S. Virgin Islands");

    }
}





