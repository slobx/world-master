package com.slobx.worldmaster;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by slobx on 5/31/2017.
 */

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 8;
    private String tabTitles[] = new String[] {"Europe", "Asia", "Africa", "North America",
            "South America", "Oceania", "Unrecognized Countries", "Territories"};
    private String keys[] = {"europeInt", "asiaInt", "africaInt", "northAmericaInt", "southAmericaInt", "oceaniaInt", "unrecognizedInt", "territoriesInt"};
    int[] images;
    private Context context;


    public MyFragmentPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        context = c;
    }



    @Override
    public Fragment getItem(int position) {

                return PageFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position] + " " + SavePref.loadInt(context, keys[position]) + "/" + GridViewUnlockAdapter.continentsIDs[position].length;
    }

}
