package com.slobx.worldmaster;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.ArrayList;

/**
 * Created by slobx on 6/1/2017.
 */

public class GridViewUnlockAdapter extends BaseAdapter {

    int continentIndex = 0;
    static int europeUnlockedCount, asiaUnlockedCount, africaUnlockedCount, northAmericaUnlockedCount, southAmericaUnlockedCount,
            oceaniaUnlockedCount, unrecognizedUnlockedCount, territoriesUnlockedCount;
    public static String image_Uri = "drawable://";
    public static ArrayList<Integer> europeLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> asiaLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> africaLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> northAmericaLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> southAmericaLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> oceaniaLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> unrecognizedLockedPositions = new ArrayList<>();
    public static ArrayList<Integer> territoriesLockedPositions = new ArrayList<>();
    public static ArrayList<ArrayList<Integer>> continentsLockedPositions = new ArrayList<>();

    String[] countriesStrings = {"europe", "asia", "africa", "northamerica", "southamerica", "oceania", "unrecognized", "territories"};

    ViewHolder holder = new ViewHolder();
    DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.ic_stub)
            .showImageForEmptyUri(R.drawable.ic_empty)
            .showImageOnFail(R.drawable.ic_error)
            .resetViewBeforeLoading(true)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .build();

    private Context mContext;
    public static int[] europeCountriesIDs = {
            R.drawable.russianfederation,
            R.drawable.france,
            R.drawable.spain,
            R.drawable.sweden,
            R.drawable.norway,
            R.drawable.finland,
            R.drawable.poland,
            R.drawable.italy,
            R.drawable.greatbritain,
            R.drawable.greece,
            R.drawable.iceland,
            R.drawable.austria,
            R.drawable.czechrepublic,
            R.drawable.denmark,
            R.drawable.switzerland,
            //from position 15 to 47 are locked
            R.drawable.ukraine,
            R.drawable.romania,
            R.drawable.belarus,
            R.drawable.kazakhstan,
            R.drawable.bulgaria,
            R.drawable.hungary,
            R.drawable.portugal,
            R.drawable.serbia,
            R.drawable.ireland,
            R.drawable.lithuania,
            R.drawable.latvia,
            R.drawable.croatia,
            R.drawable.bosniaandherzegovina,
            R.drawable.slovakia,
            R.drawable.estonia,
            R.drawable.netherlands,
            R.drawable.republicofmoldova,
            R.drawable.belgium,
            R.drawable.albania,
            R.drawable.republicofmacedonia,
            R.drawable.turkey,
            R.drawable.slovenia,
            R.drawable.cyprus,
            R.drawable.luxembourg,
            R.drawable.andorra,
            R.drawable.malta,
            R.drawable.liechtenstein,
            R.drawable.sanmarino,
            R.drawable.monaco,
            R.drawable.germany,
            R.drawable.montenegro,
            R.drawable.georgia,
            R.drawable.vatican};


    public static String[] europeCountriesInfo = {
            "Capital: Moscow \n Population: 144,163,451 \n Official language: Russian \n Currency: Russian ruble (RUB)",
            "Capital: Paris \n Population: 66,991,000 \n Official language: French \n Currency: Euro (EUR)",
            "Capital: Madrid \n Population: 46,423,064 \n Official language: Spanish \n Currency: Euro (EUR)",
            "Capital: Stockholm \n Population: 10,023,893 \n Official language: Swedish \n Currency: Swedish Krona (SEK)",
            "Capital: Oslo \n Population: 5,267,146 \n Official language: Norwegian \n Currency: Norwegian krone (NOK)",
            "Capital: Helsinki \n Population: 5,488,543 \n Official language: Finnish \n Currency: Euro (EUR)",
            "Capital: Warsaw \n Population: 38,565,443 \n Official language: Polish \n Currency: Euro (EUR)",
            "Capital: Rome \n Population: 60,599,936 \n Official language: Italian \n Currency: Euro (EUR)",
            "Capital: London \n Population: 65,485,788 \n Official language: English \n Currency: British Pound (GBP)",
            "Capital: Athens \n Population: 10,955,000 \n Official language: Greek \n Currency: Euro (EUR)",
            "Capital: Reykjavík \n Population: 10,955,000 \n Official language: Icelandic \n Currency: Icelandic króna (ISK)",
            "Capital: Vienna \n Population: 8,725,931 \n Official language: Austrian \n Currency: Euro (EUR)",
            "Capital: Prague \n Population: 10,553,948 \n Official language: Czech \n Currency: Czech koruna (CZK)",
            "Capital: Copenhagen \n Population: 5,748,769 \n Official language: Danish \n Currency: Danish krone (DKK)",
            "Capital: Bern \n Population: 8,401,120 \n Official language: German, French, Italian, Romansh \n Currency: Swiss franc (CHF)",
            //locked countries
            "Capital: Kiev \n Population: 42,541,633 \n Official language: Ukrainian \n Currency: Ukrainian hryvnia (UAH)",
            "Capital: Bucharest \n Population: 19,511,000 \n Official language: Romanian \n Currency: Romanian leu (RON)",
            "Capital: Minsk \n Population: 9,498,700 \n Official language: Belarusian \n Currency: Belarusian ruble (BYN)",
            "Capital: Astana \n Population: 18,050,488 \n Official language: Kazakh \n Currency: Tenge (KZT)",
            "Capital: Sofia \n Population: 7,101,859 \n Official language: Bulgarian \n Currency: Lev (BGN)",
            "Capital: Budapest \n Population: 9,830,485 \n Official language: Hungarian \n Currency: Forint (HUF)",
            "Capital: Lisbon \n Population: 10,341,330 \n Official language: Portuguese \n Currency: Euro (EUR)",
            "Capital: Belgrade \n Population: 7,041,599 \n Official language: Serbian \n Currency: Serbian dinar (RSD)",
            "Capital: Dublin \n Population: 4,761,865 \n Official language: Irish \n Currency: Euro (EUR)",
            "Capital: Vilnius \n Population: 2,827,947 \n Official language: Lithuanian \n Currency: Euro (EUR)",
            "Capital: Riga \n Population: 1,953,200 \n Official language: Latvian \n Currency: Euro (EUR)",
            "Capital: Zagreb \n Population: 4,190,700 \n Official language: Croatian \n Currency: Kuna (HRK)",
            "Capital: Sarajevo \n Population: 3,531,159 \n Official language: Bosnian, Croatian and Serbian \n Currency: Convertible mark (BAM)",
            "Capital: Bratislava \n Population: 5,426,252 \n Official language: Slovak \n Currency: Euro (EUR)",
            "Capital: Tallinn \n Population: 1,315,635 \n Official language: Estonian \n Currency: Euro (EUR)",
            "Capital: Amsterdam \n Population: 17,100,475 \n Official language: Dutch \n Currency: Euro (EUR)",
            "Capital: Chișinău \n Population: 2,998,235 \n Official language: Romanian \n Currency: Leu (MDL)",
            "Capital: Brussels \n Population: 11,250,585 \n Official language: Dutch, French, German \n Currency: Euro (EUR)",
            "Capital: Tirana \n Population: 2,876,591 \n Official language: Albanian \n Currency: Lek (ALL)",
            "Capital: Skopje \n Population: 2,069,162 \n Official language: Macedonian \n Currency: Macedonian denar (MKD)",
            "Capital: Ankara \n Population: 79,814,871 \n Official language: Turkish \n Currency: Turkish lira (TRY)",
            "Capital: Ljubljana \n Population: 2,065,879 \n Official language: Slovene \n Currency: Euro (EUR)",
            "Capital: Nicosia \n Population: 1,141,166 \n Official language: Greek, Turkish \n Currency: Euro (EUR)",
            "Capital: Luxembourg City \n Population: 576,249 \n Official language: Luxembourgish, French, German \n Currency: Euro (EUR)",
            "Capital: Andorra la Vella \n Population: 85,470 \n Official language: Catalan \n Currency: Euro (EUR)",
            "Capital: Valletta \n Population: 445,426 \n Official language: Maltese, English \n Currency: Euro (EUR)",
            "Capital: Vaduz \n Population: 37,340 \n Official language: German \n Currency: Swiss franc (CHF)",
            "Capital: City of San Marino \n Population: 33,285 \n Official language: Italian \n Currency: Euro (EUR)",
            "Capital: Monaco \n Population: 38,400 \n Official language: French \n Currency: Euro (EUR)",
            "Capital: Berlin \n Population: 82,175,700 \n Official language: German \n Currency: Euro (EUR)",
            "Capital: Podgorica \n Population: 678 931 \n Official language: Montenegrin \n Currency: Euro (EUR)",
            "Capital: Tbilisi \n Population: 3,720,400 \n Official language: Georgian \n Currency: Georgian lari (GEL)",
            "Capital: Vatican City \n Population: 1,000 \n Official language: Italian \n Currency: Euro (EUR)",
    };

    public static int[] asiaCountriesIDs = {
            R.drawable.afghanistan,
            R.drawable.china,
            R.drawable.india,
            R.drawable.indonesia,
            R.drawable.islamicrepublicofiran,
            R.drawable.iraq,
            R.drawable.israel,
            R.drawable.japan,
            R.drawable.malaysia,
            R.drawable.democraticpeoplesrepublicofkorea,
            R.drawable.philippines,
            R.drawable.qatar,
            R.drawable.saudiarabia,
            R.drawable.republicofkorea,
            R.drawable.unitedarabemirates,
            //from position 15 to 41 are locked
            R.drawable.azerbaijan,
            R.drawable.bahrain,
            R.drawable.bangladesh,
            R.drawable.bruneidarussalam,
            R.drawable.cambodia,
            R.drawable.armenia,
            R.drawable.bhutan,
            R.drawable.jordan,
            R.drawable.kuwait,
            R.drawable.kyrgyzstan,
            R.drawable.laopeoplesdemocraticrepublic,
            R.drawable.lebanon,
            R.drawable.maldives,
            R.drawable.mongolia,
            R.drawable.myanmar,
            R.drawable.nepal,
            R.drawable.oman,
            R.drawable.pakistan,
            R.drawable.singapore,
            R.drawable.srilanka,
            R.drawable.syrianarabrepublic,
            R.drawable.tajikistan,
            R.drawable.thailand,
            R.drawable.turkmenistan,
            R.drawable.uzbekistan,
            R.drawable.vietnam,
            R.drawable.yemen
    };
    public static String[] asiaCountriesInfo = {
            "Capital: Kabul \n Population: 33,332,025 \n Official language: Pashto, Dari \n Currency: Afghani (AFN)",
            "Capital: Beijing \n Population: 1,373,541,278 \n Official language: Standard Chinese \n Currency: Yuan (CNY)",
            "Capital: New Delhi \n Population: 1,326,572,000 \n Official language: Hindi, English \n Currency: Indian rupee (INR)",
            "Capital: Jakarta \n Population: 263,512,000 \n Official language: Indonesian \n Currency: Indonesian rupiah (IDR)",
            "Capital: Tehran \n Population: 79,966,230 \n Official language: Persian \n Currency: Rial (IRR)",
            "Capital: Baghdad \n Population: 38,146,025 \n Official language: Arabic, Kurdish \n Currency: Iraqi dinar (IQD)",
            "Capital: Jerusalem \n Population: 8,706,460 \n Official language: Hebrew, Arabic \n Currency: New shekel (ILS)",
            "Capital: Tokyo \n Population: 126,740,000 \n Official language: Japanese \n Currency: Yen (JPY)",
            "Capital: Kuala Lumpur \n Population: 31,552,000 \n Official language: Bahasa Malaysia \n Currency: Ringgit (MYR)",
            "Capital: Pyongyang \n Population: 25,155,317 \n Official language: Korean \n Currency: North Korean won (KPW)",
            "Capital: Manila \n Population: 100,981,437 \n Official language: Filipino, English \n Currency: Peso (PHP)",
            "Capital: Doha \n Population: 2,675,522 \n Official language: Arabic \n Currency: Riyal (QAR)",
            "Capital: Riyadh \n Population: 33,000,000 \n Official language: Arabic \n Currency: Saudi riyal (SAR)",
            "Capital: Seoul \n Population: 51,446,201 \n Official language: Korean \n Currency: South Korean won (KRW)",
            "Capital: Abu Dhabi \n Population: 5,927,482 \n Official language: Arabic \n Currency: UAE dirham (AED)",
            //locked countries
            "Capital: Baku \n Population: 9,823,667 \n Official language: Azerbaijani \n Currency: Manat (AZN)",
            "Capital: Manama \n Population: 1,378,000 \n Official language: Arabic \n Currency: Bahraini dinar (BHD)",
            "Capital: Dhaka \n Population: 163,187,000 \n Official language: Bengali \n Currency: Taka (BDT)",
            "Capital: Bandar Seri Begawan \n Population: 417,200 \n Official language: Malay \n Currency: Brunei dollar (BND)",
            "Capital: Phnom Penh \n Population: 15,957,223 \n Official language: Khmer \n Currency: Riel (KHR)",
            "Capital: Yerevan \n Population: 3,018,854 \n Official language: Armenian \n Currency: Dram (AMD)",
            "Capital: Thimphu \n Population: 742,737 \n Official language: Dzongkha \n Currency: Ngultrum (BTN) and Indian rupee (INR)",
            "Capital: Amman \n Population: 9,915,942 \n Official language: Arabic \n Currency: Dinar (JOD)",
            "Capital: Kuwait City \n Population: 4,348,395 \n Official language: Arabic \n Currency: Kuwaiti dinar (KWD)",
            "Capital: Bishkek \n Population: 6,019,480 \n Official language: Kyrgyz (national) and Russian (official) \n Currency: Som (KGS)",
            "Capital: Vientiane \n Population: 6,803,699 \n Official language: Lao \n Currency: Kip (LAK)",



    };

    public static int[] africaCountriesIDs = {
            R.drawable.algeria,
            R.drawable.angola,
            R.drawable.cameroon,
            R.drawable.congo,
            R.drawable.cotedivoire,
            R.drawable.egypt,
            R.drawable.ghana,
            R.drawable.kenya,
            R.drawable.mauritius,
            R.drawable.morocco,
            R.drawable.nigeria,
            R.drawable.senegal,
            R.drawable.tunisia,
            R.drawable.ethiopia,
            //from position 14 to 44 are locked
            R.drawable.benin,
            R.drawable.botswana,
            R.drawable.burkina_faso,
            R.drawable.burundi,
            R.drawable.capeverde,
            R.drawable.centralafricanrepublic,
            R.drawable.chad,
            R.drawable.comoros,
            R.drawable.equatorialguinea,
            R.drawable.eritrea,
            R.drawable.gabon,
            R.drawable.gambia,
            R.drawable.guinea,
            R.drawable.guineabissau,
            R.drawable.lesotho,
            R.drawable.liberia,
            R.drawable.madagascar,
            R.drawable.malawi,
            R.drawable.mali,
            R.drawable.mauritania,
            R.drawable.mozambique,
            R.drawable.nambia,
            R.drawable.rwanda,
            R.drawable.saotomeandprincipe,
            R.drawable.seychelles,
            R.drawable.sierraleone,
            R.drawable.somalia,
            R.drawable.sudan,
            R.drawable.swaziland,
            R.drawable.tanzania,
    };

    public static int[] northAmericaCountriesIDs = {
            R.drawable.unitedstates,
            R.drawable.bahama,
            R.drawable.mexico,
            R.drawable.costarica,
            R.drawable.cuba,
            R.drawable.jamaica,
            //from position 6 to 19 are locked
            R.drawable.barbados,
            R.drawable.trinidadandtobago,
            R.drawable.antiguaandbarbuda,
            R.drawable.panama,
            R.drawable.dominica,
            R.drawable.grenada,
            R.drawable.saintlucia,
            R.drawable.stvincentandgrenadines,
            R.drawable.belize,
            R.drawable.elsalvador,
            R.drawable.guatemala,
            R.drawable.honduras,
            R.drawable.nicaragua,
            R.drawable.haiti
    };

    public static int[] southAmericaCountriesIDs = {
            R.drawable.argentina,
            R.drawable.brazil,
            R.drawable.colombia,
            //from position 3 to 10 are locked
            R.drawable.bolivia,
            R.drawable.chile,
            R.drawable.ecuador,
            R.drawable.guyana,
            R.drawable.paraguay,
            R.drawable.peru,
            R.drawable.uruguay,
            R.drawable.venezuela
    };

    public static int[] oceaniaCountriesIDs = {
            R.drawable.australia,
            R.drawable.papuanewguinea,
            R.drawable.newzealand,
            //from position 3 to 11 are locked
            R.drawable.fiji,
            R.drawable.solomonislands,
            R.drawable.vanuatu,
            R.drawable.samoa,
            R.drawable.kiribati,
            R.drawable.tonga,
            R.drawable.marshallislands,
            R.drawable.palau,
            R.drawable.nauru
    };

    public static int[] unrecognizedCountriesIDs = {
            //from position 0 to 8 are locked
            R.drawable.abkhazia,
            R.drawable.kosovo,
            R.drawable.northerncyprus,
            R.drawable.palestine,
            R.drawable.westernsahara,
            R.drawable.southossetia,
            R.drawable.nagornokarabakhrepublic,
            R.drawable.pridnestrovianmoldavianrepublic,
            R.drawable.somaliland
    };

    public static int[] territoriesCountriesIDs = {
            //from position 0 to 50 are locked
            R.drawable.christmasisland,
            R.drawable.cocosislands,
            R.drawable.norfolkisland,
            R.drawable.easterisland,
            R.drawable.hongkong,
            R.drawable.macau,
            R.drawable.faroeislands,
            R.drawable.greenland,
            R.drawable.alandislands,
            R.drawable.frenchsouthernandantarcticlands,
            R.drawable.frenchpolynesia,
            R.drawable.wallisandfutuna,
            R.drawable.saintbarthelemy,
            R.drawable.saintpierreandmiquelon,
            R.drawable.newcaledonia,
            R.drawable.aruba,
            R.drawable.curacao,
            R.drawable.sintmaarten,
            R.drawable.bonaire,
            R.drawable.sinteustatius,
            R.drawable.saba,
            R.drawable.cookislands,
            R.drawable.niue,
            R.drawable.tokelau,
            R.drawable.alderney,
            R.drawable.guernsey,
            R.drawable.herm,
            R.drawable.isleofman,
            R.drawable.jersey,
            R.drawable.sark,
            R.drawable.anguilla,
            R.drawable.ascensionisland,
            R.drawable.bermuda,
            R.drawable.britishantarcticterritory,
            R.drawable.britishindianoceanterritory,
            R.drawable.britishvirginislands,
            R.drawable.caymanislands,
            R.drawable.falklandislands,
            R.drawable.gibraltar,
            R.drawable.montserrat,
            R.drawable.pitcairnislands,
            R.drawable.sainthelena,
            R.drawable.southgeorgiaandthesouthsandwichislands,
            R.drawable.tristandacunha,
            R.drawable.turksandcaicosislands,
            R.drawable.americansamoa,
            R.drawable.guam,
            R.drawable.northernmarianaislands,
            R.drawable.puertorico,
            R.drawable.usvirginislands,
    };


    int lockedImage = R.drawable.questionmark;

    static int[][] continentsIDs = {europeCountriesIDs, asiaCountriesIDs, africaCountriesIDs,
            northAmericaCountriesIDs, southAmericaCountriesIDs, oceaniaCountriesIDs,
            unrecognizedCountriesIDs, territoriesCountriesIDs};

    static String[][] continentsInfo = {europeCountriesInfo, asiaCountriesInfo};


    public GridViewUnlockAdapter(Context c, int continentIndex) {

        boolean isFirstTime = SavePref.loadBoolean(c, "first_time");

        this.continentIndex = continentIndex;
        mContext = c;
        initImageLoader(c);

        if (isFirstTime) {
            loadStartingInfoAboutLockedFlags();
        } else {
            loadInfoAboutLockedFlags();
        }


    }

    @Override
    public int getCount() {
        return continentsIDs[continentIndex - 1].length;
    }

    @Override
    public Object getItem(int position) {
        return continentsIDs[continentIndex - 1][position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater in = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = in.inflate(R.layout.fragment_page, null);
        holder.imageView = (ImageView) view.findViewById(R.id.countryImageUnlock);

        view.setTag(holder);

        LoadImage(position);

        return view;
    }

    public void LoadImage(int position) {


        switch (continentIndex) {
            case 1:
                if (europeLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);

                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);

                }

                break;
            case 2:
                if (asiaLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;
            case 3:
                if (africaLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;
            case 4:
                if (northAmericaLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;
            case 5:
                if (southAmericaLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;
            case 6:
                if (oceaniaLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;
            case 7:
                if (unrecognizedLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;
            case 8:
                if (territoriesLockedPositions.contains(position)) {
                    ImageLoader.getInstance().displayImage(image_Uri + lockedImage, holder.imageView, options);
                } else {
                    ImageLoader.getInstance().displayImage(image_Uri + continentsIDs[continentIndex - 1][position],
                            holder.imageView, options);
                }
                break;


        }


    }

    static class ViewHolder {
        ImageView imageView;
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); //TODO Remove for release APP

        ImageLoader.getInstance().init(config.build());
    }

    private void loadStartingInfoAboutLockedFlags() {

        for (int i = 15; i < 52; i++) {
            europeLockedPositions.add(i);
        }
        for (int i = 15; i < 42; i++) {
            asiaLockedPositions.add(i);
        }
        for (int i = 14; i < 44; i++) {
            africaLockedPositions.add(i);
        }
        for (int i = 6; i < 20; i++) {
            northAmericaLockedPositions.add(i);
        }
        for (int i = 3; i < 11; i++) {
            southAmericaLockedPositions.add(i);
        }
        for (int i = 3; i < 12; i++) {
            oceaniaLockedPositions.add(i);
        }
        for (int i = 0; i < 9; i++) {
            unrecognizedLockedPositions.add(i);
        }
        for (int i = 0; i < 50; i++) {
            territoriesLockedPositions.add(i);
        }

        europeUnlockedCount = europeCountriesIDs.length - europeLockedPositions.size();
        asiaUnlockedCount = asiaCountriesIDs.length - asiaLockedPositions.size();
        africaUnlockedCount = africaCountriesIDs.length - africaLockedPositions.size();
        northAmericaUnlockedCount = northAmericaCountriesIDs.length - northAmericaLockedPositions.size();
        southAmericaUnlockedCount = southAmericaCountriesIDs.length - southAmericaLockedPositions.size();
        oceaniaUnlockedCount = oceaniaCountriesIDs.length - oceaniaLockedPositions.size();
        unrecognizedUnlockedCount = unrecognizedCountriesIDs.length - unrecognizedLockedPositions.size();
        territoriesUnlockedCount = territoriesCountriesIDs.length - territoriesLockedPositions.size();

        continentsLockedPositions.clear();

        continentsLockedPositions.add(europeLockedPositions);
        continentsLockedPositions.add(asiaLockedPositions);
        continentsLockedPositions.add(africaLockedPositions);
        continentsLockedPositions.add(northAmericaLockedPositions);
        continentsLockedPositions.add(southAmericaLockedPositions);
        continentsLockedPositions.add(oceaniaLockedPositions);
        continentsLockedPositions.add(unrecognizedLockedPositions);
        continentsLockedPositions.add(territoriesLockedPositions);

        SavePref.saveArrayList(mContext, countriesStrings[0], europeLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[1], asiaLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[2], africaLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[3], northAmericaLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[4], southAmericaLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[5], oceaniaLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[6], unrecognizedLockedPositions);
        SavePref.saveArrayList(mContext, countriesStrings[7], territoriesLockedPositions);

        SavePref.saveInt(mContext, "europeInt", europeCountriesIDs.length - europeLockedPositions.size());
        System.out.println("SlobxCar" + (europeCountriesIDs.length - europeLockedPositions.size()));
        SavePref.saveInt(mContext, "asiaInt", asiaCountriesIDs.length - asiaLockedPositions.size());
        SavePref.saveInt(mContext, "africaInt", africaCountriesIDs.length - africaLockedPositions.size());
        SavePref.saveInt(mContext, "northAmericaInt", northAmericaCountriesIDs.length - northAmericaLockedPositions.size());
        SavePref.saveInt(mContext, "southAmericaInt", southAmericaCountriesIDs.length - southAmericaLockedPositions.size());
        SavePref.saveInt(mContext, "oceaniaInt", oceaniaCountriesIDs.length - oceaniaLockedPositions.size());
        SavePref.saveInt(mContext, "unrecognizedInt",  unrecognizedCountriesIDs.length - unrecognizedLockedPositions.size());
        SavePref.saveInt(mContext, "territoriesInt", territoriesCountriesIDs.length - territoriesLockedPositions.size());

        SavePref.saveBoolean(mContext, "first_time", false);
    }

    private void loadInfoAboutLockedFlags() {
        europeLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[0]);
        asiaLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[1]);
        africaLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[2]);
        northAmericaLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[3]);
        southAmericaLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[4]);
        oceaniaLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[5]);
        unrecognizedLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[6]);
        territoriesLockedPositions = SavePref.loadArrayList(mContext, countriesStrings[7]);

        SavePref.saveInt(mContext, "europeInt", europeCountriesIDs.length - europeLockedPositions.size());
        SavePref.saveInt(mContext, "asiaInt", asiaCountriesIDs.length - asiaLockedPositions.size());
        SavePref.saveInt(mContext, "africaInt", africaCountriesIDs.length - africaLockedPositions.size());
        SavePref.saveInt(mContext, "northAmericaInt", northAmericaCountriesIDs.length - northAmericaLockedPositions.size());
        SavePref.saveInt(mContext, "southAmericaInt", southAmericaCountriesIDs.length - southAmericaLockedPositions.size());
        SavePref.saveInt(mContext, "oceaniaInt", oceaniaCountriesIDs.length - oceaniaLockedPositions.size());
        SavePref.saveInt(mContext, "unrecognizedInt",  unrecognizedCountriesIDs.length - unrecognizedLockedPositions.size());
        SavePref.saveInt(mContext, "territoriesInt", territoriesCountriesIDs.length - territoriesLockedPositions.size());

        continentsLockedPositions.clear();

        continentsLockedPositions.add(europeLockedPositions);
        continentsLockedPositions.add(asiaLockedPositions);
        continentsLockedPositions.add(africaLockedPositions);
        continentsLockedPositions.add(northAmericaLockedPositions);
        continentsLockedPositions.add(southAmericaLockedPositions);
        continentsLockedPositions.add(oceaniaLockedPositions);
        continentsLockedPositions.add(unrecognizedLockedPositions);
        continentsLockedPositions.add(territoriesLockedPositions);

    }

    @Override

    public void notifyDataSetChanged() {
        loadInfoAboutLockedFlags();
        super.notifyDataSetChanged();
    }
}

