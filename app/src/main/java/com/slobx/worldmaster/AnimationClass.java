package com.slobx.worldmaster;

import android.graphics.Path;
import android.util.FloatMath;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.github.florent37.viewanimator.ViewAnimator;

import java.util.Random;


/**
 * Created by slobx on 5/24/2017.
 */

public class AnimationClass {

    private static final int MIN_SIZE = 1;

    public void scaleWrongAnswer(View view){
        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0.7f, 1f, 0.7f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0.6f);
        scaleAnimation.setDuration(300);
        alphaAnimation.setDuration(300);
        scaleAnimation.setFillAfter(true);
        alphaAnimation.setFillAfter(true);
        AnimationSet aSet = new AnimationSet(false);
        aSet.addAnimation(scaleAnimation);
        aSet.addAnimation(alphaAnimation);
        aSet.setFillAfter(true);
        view.startAnimation(aSet);

        view.setClickable(false);
    }

    public void correctAnswerAnimation(float translateX, float translateY, final View view) {
        view.clearAnimation();

        TranslateAnimation translateAnimation = new TranslateAnimation(0, translateX, 0, translateY);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1.5f, 1f, 1.5f);
        translateAnimation.setDuration(500);
        scaleAnimation.setDuration(500);

        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(scaleAnimation);
        view.setAnimation(animationSet);
    }

    public void wrongAnswerAnimation(View view){

        RotateAnimation rotateAnimation = new RotateAnimation(0, 360, view.getPivotX()/2, view.getPivotY()/2);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0f);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0f, 1f, 0f);

        rotateAnimation.setDuration(500);
        alphaAnimation.setDuration(500);
        scaleAnimation.setDuration(500);

        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);
        animationSet.addAnimation(alphaAnimation);

        view.startAnimation(animationSet);
    }

    public void moveFromTheScreenAnimation (View flag1, View flag2, View flag3, View flag4, View question, View clock, View score){
        TranslateAnimation upLeftTranslateAnimation = new TranslateAnimation(0, -1000, 0, 0);
        TranslateAnimation upRightTranslateAnimation = new TranslateAnimation(0, 1000, 0, 0);
        TranslateAnimation downLeftTranslateAnimation = new TranslateAnimation(0, -1000, 0, 0);
        TranslateAnimation downRightTranslateAnimation = new TranslateAnimation(0, 1000, 0, 0);
        TranslateAnimation questionAnimation = new TranslateAnimation(0, 1000, 0, 0);
        TranslateAnimation clockAnimation = new TranslateAnimation(0, -1000, 0, 0);
        TranslateAnimation scoreAnimation = new TranslateAnimation(0, 0, 0, -1000);

        upLeftTranslateAnimation.setDuration(900);
        upRightTranslateAnimation.setDuration(1750);
        downLeftTranslateAnimation.setDuration(2333);
        downRightTranslateAnimation.setDuration(3000);
        questionAnimation.setDuration(1000);
        clockAnimation.setDuration(1000);
        scoreAnimation.setDuration(500);

        upLeftTranslateAnimation.setFillAfter(true);
        upRightTranslateAnimation.setFillAfter(true);
        downLeftTranslateAnimation.setFillAfter(true);
        downRightTranslateAnimation.setFillAfter(true);
        questionAnimation.setFillAfter(true);
        clockAnimation.setFillAfter(true);
        scoreAnimation.setFillAfter(true);

        flag1.startAnimation(upLeftTranslateAnimation);
        flag2.startAnimation(upRightTranslateAnimation);
        flag3.startAnimation(downLeftTranslateAnimation);
        flag4.startAnimation(downRightTranslateAnimation);
        question.startAnimation(questionAnimation);
        clock.startAnimation(clockAnimation);
        score.startAnimation(scoreAnimation);

    }

    public void bubblePathAnimation(View view, View flag) {

        float x = view.getX() + flag.getWidth()/2 - view.getWidth()/2;
        float y = view.getY() + flag.getHeight()/2 + view.getHeight()/2;

        class Pt {
            float x, y;

            Pt(float _x, float _y) {
                x = _x;
                y = _y;
            }
        }

        Pt[] myPath = {new Pt(x-25, y-50),
                       new Pt(x-39, y-75),
                       new Pt(x-50, y-133),
                       new Pt(x-59, y-200),
                       new Pt(x-63, y-400)};

        Path path = new Path();
        path.moveTo(x, y);
        for (int i = 1; i < myPath.length; i++){
            path.lineTo(myPath[i].x, myPath[i].y);
        }

        ViewAnimator.animate(view).path(path).duration(500).start();




    }







}
